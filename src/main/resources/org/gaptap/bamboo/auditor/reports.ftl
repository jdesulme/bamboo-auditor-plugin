

<html>
<head>
	[@ui.header pageKey="auditor.admin.reports.title" title=true /]
	<meta name="decorator" content="adminpage">
    ${webResourceManager.requireResource("org.gaptap.bamboo.auditor.auditor-plugin:auditor-plugin-resources")}
</head>
<body>
	[@ui.header pageKey="auditor.admin.reports.heading" /]
	<p>
		<img src="${req.contextPath}/download/resources/org.gaptap.bamboo.bamboo-auditor-plugin:auditor-plugin-resources/images/icon_1713.png" style="float: left; margin-right: 5px" width="32" height="32" />
	</p>
	[@ww.actionmessage /]
	[@ui.clear/]
	
	<table id="reports" class="aui" width="100%">
		<thead><tr>
			<th class="labelPrefixCell">Action</th>
			<th class="valueCell">Report</th>
			<th class="valueCell">Description</th>
		</tr></thead>
		<tbody>	
			<tr>
				<td class="labelPrefixCell">
					<div class="toolbar">
						<div class="aui-toolbar inline">
							<ul class="toolbar-group">
								<li class="toolbar-item">
									<a class="toolbar-trigger" href="${req.contextPath}/plugins/servlet/permissions-report-download">[@ww.text name='auditor.admin.reports.download' /]</a>
								</li>
							</ul>
						</div>
					</div>
				</td>
				<td class="valueCell">Permissions Report</td>
				<td class="valueCell">A report of permissions configured on all Build Plans, Deployment Projects, and Deployment Environments.</td>
			</tr>
			<tr>
				<td class="labelPrefixCell">
					<div class="toolbar">
						<div class="aui-toolbar inline">
							<ul class="toolbar-group">
								<li class="toolbar-item">
									<a class="toolbar-trigger" href="${req.contextPath}/plugins/servlet/repository-report-download">[@ww.text name='auditor.admin.reports.download' /]</a>
								</li>
							</ul>
						</div>
					</div>
				</td>
				<td class="valueCell">Repository Report</td>
				<td class="valueCell">A report of source repositories configured on all Build Plans.</td>
			</tr>
		</tbody>
	</table>
</body>
</html>
