/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.auditor.alerts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.gaptap.bamboo.auditor.BaseAdminAction;
import org.gaptap.bamboo.auditor.scheduler.SchedulerService;
import org.quartz.JobDetail;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerUtils;

import com.atlassian.bamboo.notification.Notification;
import com.atlassian.bamboo.notification.NotificationManager;
import com.atlassian.bamboo.notification.NotificationRecipient;

/**
 * @author David Ehringer
 */
public class AlertsAdminAction extends BaseAdminAction {

    private final SchedulerService schedulerService;

    private NotificationManager notificationManager;
    private Map<String, NotificationRecipient> notificationRecipients = new TreeMap<String, NotificationRecipient>();

    public AlertsAdminAction(SchedulerService schedulerService,  NotificationManager notificationManager) {
        this.schedulerService = schedulerService;
        this.notificationManager= notificationManager;
    }

    public String doEnableExample() throws SchedulerException {
        System.out.println("Enable Example");
        JobDetail jobDetail = new JobDetail("Example", "Example", ExampleJob.class);
        Trigger trigger = TriggerUtils.makeSecondlyTrigger(20);
        trigger.setName("ExampleTrigger");
        trigger.setStartTime(new Date());
        schedulerService.scheduleJob(jobDetail, trigger);
        return SUCCESS;
    }

    /**
     * Method to get notification recipients for the ui
     * 
     * @return List of all {@link NotificationRecipient} types to display on the
     *         UI
     */
    public List<NotificationRecipient> getAllNotificationRecipientTypes() {
        List<NotificationRecipient> recipients = new ArrayList<NotificationRecipient>();
        recipients.addAll(getAvailableRecipientTypes().values());
        Collections.sort(recipients);
        return recipients;
    }

    public Map<String, NotificationRecipient> getAvailableRecipientTypes() {
        if (notificationRecipients.isEmpty()) {
            List<NotificationRecipient> recipientObjects = notificationManager.getAllNotificationRecipients();
            
            for (NotificationRecipient recipient : recipientObjects) {
                notificationRecipients.put(recipient.getKey(), recipient);
            }
        }
        return notificationRecipients;
    }
    
}
