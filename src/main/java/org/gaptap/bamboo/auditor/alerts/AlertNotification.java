/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.auditor.alerts;

import java.util.Set;

import com.atlassian.bamboo.notification.AbstractNotification;
import com.atlassian.bamboo.notification.Notification;
import com.atlassian.bamboo.notification.NotificationRecipient;
import com.atlassian.event.Event;

/**
 * @author David Ehringer
 */
public class AlertNotification extends AbstractNotification{

    @Override
    public String getDescription() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getEmailSubject() throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getHtmlEmailContent() throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getIMContent() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getTextEmailContent() throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

}
