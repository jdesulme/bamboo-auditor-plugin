/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.auditor.report.repository.parser;

import org.gaptap.bamboo.auditor.report.repository.RepositoryParser;
import org.gaptap.bamboo.auditor.report.repository.RepositoryTableRow;

import com.atlassian.bamboo.repository.AuthenticationType;
import com.atlassian.bamboo.repository.Repository;
import com.atlassian.bamboo.repository.svn.SvnRepository;

/**
 * @author David Ehringer
 */
public class SvnRepositoryParser implements RepositoryParser {

    @Override
    public boolean canParse(Repository repository) {
        return SvnRepository.COMPLETE_PLUGIN_KEY.equals(repository.getKey());
    }

    @Override
    public void parse(Repository repository, RepositoryTableRow row) {
        SvnRepository svn = (SvnRepository) repository;

        row.setRepositoryType("SVN");
        row.setRepositoryUrl(svn.getRepositoryUrl());
        row.setAuthType(svn.getAuthType());
        if (AuthenticationType.PASSWORD.getKey().equals(svn.getAuthType())) {
            row.setCredentials(svn.getUsername());
        } else if (AuthenticationType.SSH.getKey().equals(svn.getAuthType())) {
            row.setCredentials(svn.getKeyFile());
        } else if (AuthenticationType.SSL_CLIENT_CERTIFICATE.getKey().equals(svn.getAuthType())) {
            row.setCredentials(svn.getKeyFile());
        }
    }

}
