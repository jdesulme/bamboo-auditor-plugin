/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.auditor.report.permissions;

import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.gaptap.bamboo.auditor.report.CsvFormatter;

import com.atlassian.bamboo.configuration.AdministrationConfiguration;
import com.atlassian.bamboo.deployments.projects.service.DeploymentProjectService;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.security.acegi.acls.HibernateMutableAclService;
import com.atlassian.spring.container.ContainerManager;

/**
 * @author David Ehringer
 */
public class PermissionsReportDownloadServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    private final CsvFormatter formatter = new CsvFormatter();
    private final HibernateMutableAclService mutableAclService;
    private final PlanManager planManager;
    private final DeploymentProjectService deploymentProjectService;

    public PermissionsReportDownloadServlet(PlanManager planManager, DeploymentProjectService deploymentProjectService,
            HibernateMutableAclService mutableAclService) {
        this.planManager = planManager;
        this.deploymentProjectService = deploymentProjectService;
        this.mutableAclService = mutableAclService;
    }

    public AdministrationConfiguration getAdministrationConfiguration() {
        return (AdministrationConfiguration) ContainerManager.getComponent("administrationConfiguration");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/csv");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + getFileName() + "\"");
        try {
            OutputStream outputStream = response.getOutputStream();
            outputStream.write(buildReport().getBytes());
            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    private String getFileName() {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        String date = format.format(new Date());
        return "bamboo-permissions-audit-report-" + date + ".csv";
    }

    private String buildReport() {
        PermissionsReport report = new PermissionsReport(mutableAclService, planManager, deploymentProjectService,
                getAdministrationConfiguration());
        report.generate();
        return formatter.format(report);
    }

}
