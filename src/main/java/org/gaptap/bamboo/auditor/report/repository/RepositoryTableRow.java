/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.auditor.report.repository;

import java.util.List;

import org.gaptap.bamboo.auditor.report.TableRow;

import com.google.common.collect.Lists;

/**
 * @author David Ehringer
 */
public class RepositoryTableRow implements TableRow {

    public static final List<String> COLUMN_HEADINGS = Lists.newArrayList("Project", "Project Key", "Plan", "Plan Key",
            "Repository Type", "Repository URL", "Authentication Type", "Credentials", "Configuration Link");;

    private String project;
    private String projectKey;
    private String plan;
    private String planKey;
    private String repositoryType;
    private String repositoryUrl;
    private String authType;
    private String credentials;

    private final String baseUrl;

    public RepositoryTableRow(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Override
    public List<String> getData() {
        return Lists.newArrayList(project, projectKey, plan, planKey, repositoryType, repositoryUrl, authType,
                credentials, getConfigurationLink());
    }

    public String getConfigurationLink() {
        return baseUrl + "/chain/admin/config/editChainRepository.action?buildKey=" + getPlanKey();
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getRepositoryType() {
        return repositoryType;
    }

    public void setRepositoryType(String repositoryType) {
        this.repositoryType = repositoryType;
    }

    public String getRepositoryUrl() {
        return repositoryUrl;
    }

    public void setRepositoryUrl(String repositoryUrl) {
        this.repositoryUrl = repositoryUrl;
    }

    public String getAuthType() {
        return authType;
    }

    public void setAuthType(String authType) {
        this.authType = authType;
    }

    public String getCredentials() {
        return credentials;
    }

    public void setCredentials(String credentials) {
        this.credentials = credentials;
    }

    public String getProjectKey() {
        return projectKey;
    }

    public void setProjectKey(String projectKey) {
        this.projectKey = projectKey;
    }

    public String getPlanKey() {
        return planKey;
    }

    public void setPlanKey(String planKey) {
        this.planKey = planKey;
    }

}
