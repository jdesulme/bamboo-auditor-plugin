/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.auditor.scheduler;

import java.util.Date;
import java.util.Properties;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import com.atlassian.bamboo.quartz.AutowiringJobFactory;

/**
 * @author David Ehringer
 */
public class DefaultSchedulerService implements SchedulerService, DisposableBean {

    private final SchedulerFactoryBean factoryBean;
    private final Scheduler scheduler;

    public DefaultSchedulerService() throws Exception {
        factoryBean = new SchedulerFactoryBean();

        Properties quartzProperties = new Properties();
        quartzProperties.put("org.quartz.scheduler.skipUpdateCheck", "true");
        quartzProperties.put("org.quartz.threadPool.class", "com.atlassian.bamboo.quartz.SystemAuthorizedThreadPool");
        factoryBean.setQuartzProperties(quartzProperties);

        factoryBean.setJobFactory(new AutowiringJobFactory());
        factoryBean.afterPropertiesSet();
        scheduler = (Scheduler) factoryBean.getObject();
    }

    @Override
    public Date scheduleJob(JobDetail jobDetail, Trigger trigger) throws SchedulerException {
        return scheduler.scheduleJob(jobDetail, trigger);
    }

    @Override
    public boolean unscheduleJob(String triggerName, String groupName) throws SchedulerException {
        return scheduler.unscheduleJob(triggerName, groupName);
    }

    @Override
    public void destroy() throws Exception {
        factoryBean.destroy();
    }

    @Override
    public Scheduler getScheduler() {
        return scheduler;
    }

}
